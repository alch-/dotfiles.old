set nocompatible
filetype off




call plug#begin('~/.config/nvim/autoload')

Plug 'shougo/deoplete.nvim'
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/syntastic'
Plug 'jnurmine/zenburn'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'kien/ctrlp.vim'
Plug 'zchee/deoplete-jedi'
Plug 'shougo/neosnippet.vim'

set nu

" python stuff
au BufNewFile,BufRead *.py
   \ set tabstop=4
   \ set softtabstop=4
   \ set shiftwidth=4
   \ set textwidth=79
   \ set expandtab
   \ set autoindent
   \ set fileformat=unix

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_python_checkers = ['flake8']
let g:syntastic_sql_checkers = ['sqlint']

let g:deoplete#enable_at_startup = 1

colorscheme zenburn

let python_highlight_all = 1
syntax on

map <C-n> :NERDTreeToggle<CR>
let NerdTreeIgnore=['\.pyc$']
let g:airline_powerline_fonts = 1
call plug#end()
