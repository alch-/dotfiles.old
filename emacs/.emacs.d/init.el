(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(cyberpunk-theme
     zenburn-theme
     monokai-theme
     elpy
     powerline
     neotree
     flycheck
     py-autopep8
     ido-ubiquitous
     ido-vertical-mode
     helm
     org-bullets
     auto-complete
     popup
     projectile
     ))



(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      myPackages)

;;org-mode settings
;;(require 'ox-md)
(setq org-directory "~/Notes")
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)

(setq org-agenda-files (list "~/Notes/work.org"
			     "~/Notes/university.org"
			     "~/Notes/stuff.org"))


;;neotree settings
(require 'neotree)
(global-set-key [f9] 'neotree-toggle)

;;powrline settings
(require 'powerline)
(powerline-default-theme)

;;(setq inhibit-startup-message t)

;;python settings
(elpy-enable)

;;visual settings
(load-theme 'monokai t)
(global-linum-mode t)


(require 'auto-complete)
(ac-config-default)
(require 'popup)

(require 'helm)
(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)


;;move buffers with windowmove
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(setq show-paren-delay 0)
(show-paren-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)
(blink-cursor-mode 0)

;;indent with spaces instead of tabs
(setq-default indent-tabs-mode nil)


(setq backup-directory-alist '(("" . "~/.emacs-backup")))
